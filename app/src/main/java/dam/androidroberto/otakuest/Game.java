package dam.androidroberto.otakuest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class Game extends AppCompatActivity {

    private Button btComprobar;
    private TextView tvPalabra, tvPalabra2, tvPalabra3, tvPalabra4, tvPalabra5;
    private TextView[] tvs;
    private EditText etPalabra;
    private char[] letras;
    private int intento;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        setUI();
    }

    private void setUI() {
        intento = 0;
        btComprobar = findViewById(R.id.btComprobar);
        tvPalabra = findViewById(R.id.tvPalabra);
        tvPalabra2 = findViewById(R.id.tvPalabra2);
        tvPalabra3 = findViewById(R.id.tvPalabra3);
        tvPalabra4 = findViewById(R.id.tvPalabra4);
        tvPalabra5 = findViewById(R.id.tvPalabra5);
        tvs = new TextView[5];
        tvs[0] = tvPalabra;
        tvs[1] = tvPalabra2;
        tvs[2] = tvPalabra3;
        tvs[3] = tvPalabra4;
        tvs[4] = tvPalabra5;
        etPalabra = findViewById(R.id.etPalabra);
        Bundle bundle = getIntent().getExtras();
        letras = bundle.getString("palabra").toCharArray();
        InitArray(tvs);
        btComprobar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etPalabra.getText().toString().length() == letras.length) {
                    StringBuilder sb = new StringBuilder();
                    char[] jugada = etPalabra.getText().toString().toLowerCase(Locale.ROOT).toCharArray();
                    for (int i = 0; i < letras.length; i++) {
                        if (jugada[i] == letras[i])
                            sb.append("<font color='#09EE13'>").append(String.valueOf(jugada[i]).toUpperCase(Locale.ROOT)).append(" </font>");
                        else if (existe(jugada[i]))
                            sb.append("<font color='#FFE20C'>").append(String.valueOf(jugada[i]).toUpperCase(Locale.ROOT)).append(" </font>");
                        else
                            sb.append("<font color='#757575'>").append(String.valueOf(jugada[i]).toUpperCase(Locale.ROOT)).append(" </font>");
                    }
                    tvs[intento].setText(Html.fromHtml(sb.toString()));
                    etPalabra.setText("");
                    intento++;
                    if (win(jugada)) {
                        MainActivity.USER.increaseWins();
                        Bundle b = new Bundle();
                        b.putBoolean("win", true);
                        b.putString("categoria", bundle.getString("categoria"));
                        startActivity(new Intent(Game.this, End.class).putExtras(b));
                    } else if (intento == 5) {
                        Bundle b = new Bundle();
                        b.putBoolean("win", false);
                        b.putString("categoria", bundle.getString("categoria"));
                        startActivity(new Intent(Game.this, End.class).putExtras(b));
                    }
                } else {
                    Toast.makeText(Game.this, "Introduzca una palabra de " + letras.length + " letras", Toast.LENGTH_SHORT).show();
                }
            }
            private boolean existe(char c) {
                boolean existe = false;
                for (char letra : letras) {
                    if (c == letra) {
                        existe = true;
                        break;
                    }
                }
                return existe;
            }

            private boolean win(char[] jugada) {
                boolean win = true;
                boolean[] b = new boolean[letras.length];
                for (int i = 0; i < letras.length; i++) {
                    win = win && (jugada[i] == letras[i]);
                }
                return win;
            }
        });
    }

    private void InitArray(TextView[] tvs) {
        for (int i = 0; i < tvs.length; i++) {
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j <letras.length; j++) {
                sb.append("_ ");
            }
            tvs[i].setText(sb.toString());
        }
    }
}