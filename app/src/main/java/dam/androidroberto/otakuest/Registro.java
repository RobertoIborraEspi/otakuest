package dam.androidroberto.otakuest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;


import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class Registro extends AppCompatActivity {
    EditText etUserName, etMail, etBiDate, etPassword, etPassword2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        setUI();
    }

    private void setUI() {
        etUserName = findViewById(R.id.etPersonName);
        etMail = findViewById(R.id.etUsr);
        etBiDate = findViewById(R.id.etBiDate);
        etPassword = findViewById(R.id.etPwd);
        etPassword2 = findViewById(R.id.etPassword2);
    }

    public void registro(View v) {
        if (etPassword.getText().toString().equals(etPassword2.getText().toString())) {
            String usr = etUserName.getText().toString().toLowerCase(Locale.ROOT);
            Map<String, Object> map = new HashMap();
            map.put("username", usr);
            map.put("password", etPassword.getText().toString());
            map.put("email", etMail.getText().toString().toLowerCase(Locale.ROOT));
            map.put("birth", etBiDate.getText().toString());
            map.put("wins", 0);
            MainActivity.BD.collection("Users").document(usr).set(map).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void unused) {
                    Toast.makeText(Registro.this, "¡Registro completado con exito!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(Registro.this, "Error en el proceso de registro", Toast.LENGTH_SHORT).show();
                }
            });
        } else
            Toast.makeText(this, "Los campos contraseña no coinciden... La contraseña debe coincidir", Toast.LENGTH_SHORT).show();


    }

    public void pickDate(View view) {
        int day, month, year;
        if (etBiDate.length() > 1) {
            day = Integer.parseInt(etBiDate.getText().toString().split("-")[2]);
            month = Integer.parseInt(etBiDate.getText().toString().split("-")[1]) - 1;
            year = Integer.parseInt(etBiDate.getText().toString().split("-")[0]);
        } else {
            final Calendar c = Calendar.getInstance();
            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DAY_OF_MONTH);
        }

        DatePickerDialog dpd = new DatePickerDialog(Registro.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int pYear, int pMonth, int pDay) {
                StringBuilder sb = new StringBuilder();
                if (pDay >= 10) sb.append(pDay);
                else sb.append("0").append(pDay);
                if (pMonth >= 9) sb.append("-").append(pMonth + 1).append("-").append(pYear);
                else sb.append("-").append("0").append(pMonth + 1).append("-").append(pYear);

                etBiDate.setText(sb.toString());
            }
        }, year, month, day);
        dpd.show();
    }
}