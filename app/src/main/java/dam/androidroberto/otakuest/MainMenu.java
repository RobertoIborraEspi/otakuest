package dam.androidroberto.otakuest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;

public class MainMenu extends AppCompatActivity {


    private TextView tvWins;
    private Button btLogout, btPlay, btPlay2, btPlay3, btPlay4, btPlay5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        setUI();
    }

    private void setUI() {
        tvWins = findViewById(R.id.tvWins);
        tvWins.setText("Partidas ganadas: " + MainActivity.USER.getWins());
        btLogout = findViewById(R.id.btLogout);
        btLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.USER = null;
                SharedPreferences.Editor editor;
                editor = MainActivity.prefs.edit();
                editor.clear();
                editor.commit();
                finish();
            }
        });
        btPlay = findViewById(R.id.btPlay);
        btPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGame("rdm");
            }
        });
        btPlay2 = findViewById(R.id.btPlay2);
        btPlay2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGame("DragonBall");
            }
        });
        btPlay3 = findViewById(R.id.btPlay3);
        btPlay3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGame("Naruto");
            }
        });
        btPlay4 = findViewById(R.id.btPlay4);
        btPlay4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGame("OnePiece");
            }
        });
        btPlay5 = findViewById(R.id.btPlay5);
        btPlay5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGame("Pokemon");
            }
        });
    }

    private void startGame(String categoria) {
        if (categoria.equals("rdm")) {
            MainActivity.BD.collection("Palabras").document("Nombres").get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    int w = (int) ((Math.random() * documentSnapshot.getData().size()) + 1);
                    String cat = documentSnapshot.get(String.valueOf(w)).toString();
                    MainActivity.BD.collection("Palabras").document(cat).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if (documentSnapshot.exists()){
                                int w = (int) ((Math.random() * documentSnapshot.getData().size()) + 1);
                                String p = documentSnapshot.get(String.valueOf(w)).toString();
                                Bundle b = new Bundle();
                                b.putString("palabra", p);
                                b.putString("categoria", categoria);
                                startActivity(new Intent(MainMenu.this, Game.class).putExtras(b));
                            }
                            else
                                Toast.makeText(MainMenu.this, "Colección inexistente", Toast.LENGTH_SHORT).show();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(MainMenu.this, "No ha sido posible iniciar el juego", Toast.LENGTH_SHORT).show();
                            Log.e("Error-Juego", e.getMessage());
                        }
                    });
                }
            });
        } else {
            MainActivity.BD.collection("Palabras").document(categoria).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    if (documentSnapshot.exists()){
                        int w = (int) ((Math.random() * documentSnapshot.getData().size()) + 1);
                        String p = documentSnapshot.get(String.valueOf(w)).toString();
                        Bundle b = new Bundle();
                        b.putString("palabra", p);
                        b.putString("categoria", categoria);
                        startActivity(new Intent(MainMenu.this, Game.class).putExtras(b));
                    }
                    else
                        Toast.makeText(MainMenu.this, "Colección inexistente", Toast.LENGTH_SHORT).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(MainMenu.this, "No ha sido posible iniciar el juego", Toast.LENGTH_SHORT).show();
                    Log.e("Error-Juego", e.getMessage());
                }
            });
        }
    }
}