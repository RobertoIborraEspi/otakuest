package dam.androidroberto.otakuest.models;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;

import dam.androidroberto.otakuest.MainActivity;

public class User {
   private String name;
   private int wins;

   public User() {
      // Default constructor required for calls to DataSnapshot.getValue(User.class)
   }

   public User(String name, int wins) {
      this.name = name;
      this.wins = wins;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public int getWins() {
      return wins;
   }

   public void setWins(int wins) {
      this.wins = wins;
   }

   public void increaseWins() {
      this.wins++;
      DocumentReference docRef = MainActivity.BD.collection("Users").document(this.name);
      docRef.update("wins", this.wins);
   }
}
