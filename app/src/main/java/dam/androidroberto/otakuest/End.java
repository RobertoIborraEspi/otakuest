package dam.androidroberto.otakuest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;

public class End extends AppCompatActivity {

    private TextView tvResult, tvWins2;
    private Button btMainMenu, btPlayAgain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end);

        setUI();
    }

    private void setUI() {
        tvResult = findViewById(R.id.tvResult);
        tvWins2 = findViewById(R.id.tvWins2);
        btMainMenu = findViewById(R.id.btMainMenu);
        btPlayAgain = findViewById(R.id.btPlayAgain);
        Bundle b = getIntent().getExtras();
        if (b.getBoolean("win")) {
            tvResult.setText("¡¡Has ganado!!");
            tvResult.setTextColor(Color.GREEN);
        } else {
            tvResult.setText("Has perdido");
            tvResult.setTextColor(Color.RED);
        }
        tvWins2.setText("Partidas ganadas: " + MainActivity.USER.getWins());
        btMainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(End.this, MainMenu.class));
            }
        });
        btPlayAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGame(b.getString("categoria"));
            }
        });
    }
    private void startGame(String categoria) {
        if (categoria.equals("rdm")) {
            MainActivity.BD.collection("Palabras").document("Nombres").get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    int w = (int) ((Math.random() * documentSnapshot.getData().size()) + 1);
                    String cat = documentSnapshot.get(String.valueOf(w)).toString();
                    MainActivity.BD.collection("Palabras").document(cat).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if (documentSnapshot.exists()){
                                int w = (int) ((Math.random() * documentSnapshot.getData().size()) + 1);
                                String p = documentSnapshot.get(String.valueOf(w)).toString();
                                Bundle b = new Bundle();
                                b.putString("palabra", p);
                                startActivity(new Intent(End.this, Game.class).putExtras(b));
                            }
                            else
                                Toast.makeText(End.this, "Colección inexistente", Toast.LENGTH_SHORT).show();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(End.this, "No ha sido posible iniciar el juego", Toast.LENGTH_SHORT).show();
                            Log.e("Error-Juego", e.getMessage());
                        }
                    });
                }
            });
        } else {
            MainActivity.BD.collection("Palabras").document(categoria).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    if (documentSnapshot.exists()){
                        int w = (int) ((Math.random() * documentSnapshot.getData().size()) + 1);
                        String p = documentSnapshot.get(String.valueOf(w)).toString();
                        Bundle b = new Bundle();
                        b.putString("palabra", p);
                        startActivity(new Intent(End.this, Game.class).putExtras(b));
                    }
                    else
                        Toast.makeText(End.this, "Colección inexistente", Toast.LENGTH_SHORT).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(End.this, "No ha sido posible iniciar el juego", Toast.LENGTH_SHORT).show();
                    Log.e("Error-Juego", e.getMessage());
                }
            });
        }
    }
}