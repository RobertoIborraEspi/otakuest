package dam.androidroberto.otakuest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.Locale;

import dam.androidroberto.otakuest.models.User;

public class Login extends AppCompatActivity {

    private EditText etUsr, etPwd;
    private Button btLogin;
    private CheckBox cbRecordarme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        SetUI();
    }


    private void SetUI() {
        etUsr = findViewById(R.id.etUsr);
        etPwd = findViewById(R.id.etPwd);
        cbRecordarme = findViewById(R.id.cbRecordarme);
        btLogin = findViewById(R.id.btStart);
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if (!etUsr.getText().toString().equals("")) {
                   if (!etPwd.getText().toString().equals("")) {
                       String usr = etUsr.getText().toString().toLowerCase(Locale.ROOT);
                       String pwd = etPwd.getText().toString();

                       MainActivity.BD.collection("Users").document(usr).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                           @Override
                           public void onSuccess(DocumentSnapshot documentSnapshot) {
                               if (documentSnapshot.exists()){
                                   if (pwd.equals(documentSnapshot.get("password").toString())) {
                                       Toast.makeText(Login.this, "Login completado", Toast.LENGTH_SHORT).show();
                                       MainActivity.USER = new User(documentSnapshot.get("username").toString(), Integer.parseInt(documentSnapshot.get("wins").toString()));
                                       if (cbRecordarme.isChecked()) {
                                           SharedPreferences.Editor editor;
                                           editor = MainActivity.prefs.edit();
                                           editor.putString("username", documentSnapshot.get("username").toString());
                                           editor.commit();
                                       }
                                       finish();
                                   } else
                                       Toast.makeText(Login.this, "Contraseña incorrecta", Toast.LENGTH_SHORT).show();
                               }
                               else
                                   Toast.makeText(Login.this, "Usuario inexistente", Toast.LENGTH_SHORT).show();
                           }
                       }).addOnFailureListener(new OnFailureListener() {
                           @Override
                           public void onFailure(@NonNull Exception e) {
                               Toast.makeText(Login.this, "Error en el login", Toast.LENGTH_SHORT).show();
                               Log.e("Error-Login", e.getMessage());
                           }
                       });
                   } else
                       Toast.makeText(Login.this, "El campo de contraseña no puede estar vacio", Toast.LENGTH_SHORT).show();
               } else
                   Toast.makeText(Login.this, "El campo de usuario no puede estar vacio", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void registro(View v){
        startActivity(new Intent(this, Registro.class));
    }

}