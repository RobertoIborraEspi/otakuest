package dam.androidroberto.otakuest;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import dam.androidroberto.otakuest.models.User;

public class MainActivity extends AppCompatActivity {


    private Button btStart;
    public static FirebaseFirestore BD;
    public static User USER;
    public static SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {
        btStart = findViewById(R.id.btStart);
        prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
        BD = FirebaseFirestore.getInstance();
        btStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = prefs.getString("username", "unknown");
                if (username.equals("unknown")) {
                    if (USER == null){
                        Toast.makeText(MainActivity.this, "Logueate para comenzar", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(MainActivity.this, Login.class));
                    } else {
                        Toast.makeText(MainActivity.this, "¡Bienvenido!", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(MainActivity.this, MainMenu.class));
                    }
                } else {
                    BD.collection("Users").document(username).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            MainActivity.USER = new User(documentSnapshot.get("username").toString(), Integer.parseInt(documentSnapshot.get("wins").toString()));
                            Toast.makeText(MainActivity.this, "¡Bienvenido!", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(MainActivity.this, MainMenu.class));
                        }
                    });
                }
            }
        });
    }

}